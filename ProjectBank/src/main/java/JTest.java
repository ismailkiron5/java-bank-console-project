import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

public class JTest {

	public static FoodDao fm = new FoodDao();
	
	
	@BeforeClass //this used be called Before in jUnit 4
	public static void setUp() {
		//System.out.println("-----------Before Method------------");
		//fm =
		fm.Connection("fooduser", "admin");


	}

	@Test
	public void validaccounttest() {
		
		//System.out.println("asi");

		
		assertEquals(false, fm.validaccount("54564654"), " Valid Account Testing"); //assertEquals(expectedValue, actualValue, fail string)
		assertEquals(true, fm.validaccount("205417"), "Valid Account Testing"); //assertEquals(expectedValue, actualValue, fail string)


		//return false;
	}
	
	@Test
	public void validbalancetest() {
		
		//fm.Connection("fooduser", "admin");

		
		assertEquals(true, fm.validbalance("205417", "100"));
		assertEquals(false, fm.validbalance("314823", "1000000"));
		assertEquals(false, fm.validbalance("205417", "1000000"));

	}
	
	
	
}
