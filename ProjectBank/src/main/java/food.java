import java.sql.Connection;
import java.util.ArrayList;

public interface food {
	
	//public static final Connection conn = null;
	String CSloggedin(String auth);
	void Connection(String user,String password);
	String account_create(String name,String email,String password);
	//void loggedin();
	String loggedin(String name, String password);
	public String account_info(String account);
	
	public String customer_request(String amount,String com,String acc);
	public String Cus_sendmoney(String receive,String amount,String acc);
	public String deposit(String amount,String acc);
	public ArrayList<String> employeehome(String acc);
	public String employeeapprove(String acc);
	public String employeeapprovebyname(String name);
	//public String Adminhome(String auth);
	public String employeecustomer(String auth);
	public String employeedeny(String auth);
	boolean validaccount(String acc) ;
	public boolean validbalance(String sender,String bal);
	public String Adminsendmoney(String send,String rec,String bal);
	public String Adminwithdraw(String acc,String bal);
	public String admindeposit(String acc,String bal);
	public ArrayList<String> adminview();
	public String adminuser(String user);
}
